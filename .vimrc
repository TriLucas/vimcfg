" setup pathogen
execute pathogen#infect()
filetype off
syntax on
filetype plugin indent on

" load local-specific content
source ~/.vim/local.vim

" highliht / dehighlight matches with F4
set hlsearch
let hlstate=0
nnoremap <F4> :if (hlstate == 0) \| nohlsearch \| else \| set hlsearch \| endif \| let hlstate=1-hlstate<cr>

" Find responsible tox.ini file
function Find_tox_ini(current_folder)
    let folder = a:current_folder
    let found = ''
    while found !~ 'tox.ini' && folder != '/'
        let found = findfile('tox.ini', folder)
        let folder = fnamemodify(folder, ':h')
    endwhile
    return fnamemodify(found, ':p:h')
endfunction

" Set current working directory to where the responsible tox.ini is located 
function Cd_flake8()
    let current = getcwd()
    try
    let fpath = expand('%:p:h')
        let path = Find_tox_ini(fpath)
        cd `=path`
    catch
    endtry
    try
        call Flake8()
    catch
    endtry
    cd `=current`
endfunction

" Map manual .py flaking to F3
autocmd FileType python map <buffer> <F3> :call Cd_flake8() <CR>

" initialize cwd to root_dir from local.vim
cd `=root_dir`

" colorize the 80th column
set colorcolumn=80

" Set swap-folder
set directory=$HOME/.vim/swaps//

" set relative line-numbers
set rnu

" gvim specifics
if has('gui_running')
    set guioptions -=m
    set guioptions -=T
    set guioptions -=L
    set guioptions -=r

    colorscheme evening
endif

" Prepare font-configuration from local.vim
let normal_font = used_font . ' ' . normal_size
let big_font = used_font . ' ' . big_size

" Set font
let &guifont=normal_font

" map F5 to switch between fonts
let is_big = 0
nnoremap <F5> :if (is_big == 0) \| let &guifont=big_font \| else \| let &guifont=normal_font \| endif \| let is_big=1-is_big<cr>

" call flake8 on .py-write
autocmd BufWritePost *.py call Cd_flake8()

" shutdown audiobell
set visualbell

" Enable proper mouse support
set mouse=a

" Enable proper tab-completion
set wildmode=longest,list,full
set wildmenu
