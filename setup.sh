#!/bin/bash
if [ $# -eq 0 ] ; then
  FOLDER='.vim'
else
  FOLDER=$1
fi

echo "Fetching submodules..."
git submodule init
git submodule update

echo "Installing pip-requirements..."
pip install -r requirements.txt

echo "Creating Folders..."
mkdir -p ~/$FOLDER/autoload
mkdir -p ~/$FOLDER/bundle
mkdir -p ~/$FOLDER/swaps

echo "Linking plugins..."
ln -sr vim-pathogen/autoload/pathogen.vim ~/$FOLDER/autoload/pathogen.vim
mkdir -p ~/$FOLDER/bundle/flake8/plugin
ln -sr vim-flake8/autoload/flake8.vim ~/$FOLDER/bundle/flake8/plugin/flake8.vim
ln -sr vim-flake8/ftplugin/python_flake8.vim ~/$FOLDER/bundle/flake8/plugin/python_flake8.vim

echo "Copying config file..."
cp local.vim.example ~/$FOLDER/local.vim
