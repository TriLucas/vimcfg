# My (G)VIM configuration

## Trivial

This configuration installs pathogen and vim-flake8, set's the tabbing configuration to adhere to PEP-8 standards (4 spaces instead of tabs) and displays a red marker at the 80th column.

## Installation

Clone the repository to a location of your choice.
**My Recommendation** would be to let `~/.vim` be the repository:

```
$ git clone https://gitlab.com/TriLucas/vimcfg.git ~/.vim
```

Run the setup-script:

```
$ setup.sh [folder to install to, defaults to .vim]
```

If you specify a folder, you may have to configure vim to use the created config file yourself.

**Warning**: `setup.sh` overwrites everything that may already be present in the given folder.

## Configuration

Adjust the local.vim file to adhere to your requirements:

* `root_dir` defines the folder where all your projects are supposed to be
* `used_font` defines the font vim should use
* `normal_size` defines the font size vim should start with
* `big_size` defines the size vim should switch to on toggling

## Usage

### Flake checking

Flake8 is run everytime a .py file is saved. Alternatively, you can hit F3 to trigger a Flake8 check manually.

### Font size adjustment

Hit F5 to switch between the 2 configured font sizes.

### Search match highlighting

Hit F4 to toggle search match highlighting.
